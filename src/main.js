import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'

import BaseButton from '@/UI/TheBaseButton'
import BaseInput from '@/UI/TheBaseInput'
import BaseCard from '@/UI/TheBaseCard'
import MessageModal from '@/components/Modals/MessageModal'
import SpinnerCircle from '@/components/SpinnerCircle'

Vue.config.productionTip = false

Vue.component('base-button', BaseButton)
Vue.component('base-input', BaseInput)
Vue.component('base-card', BaseCard)
Vue.component('MessageModal', MessageModal)
Vue.component('spinner-circle', SpinnerCircle)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
