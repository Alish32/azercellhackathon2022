import axios from 'axios';
import { BASE_URL } from './Variables'
const API_URL_CATEGORIES= `${BASE_URL}`

export const CATEGORIES = {
    categories() {
        const url = `${API_URL_CATEGORIES}/categories`;
        return axios.get(url);
    },
}