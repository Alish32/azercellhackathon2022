import axios from 'axios';
import store from '@/store/index'
import router from '@/router/index'


import { AUTH } from './Auth'
import { CATEGORIES } from './Categories'
import { INTERESTS } from './Interests';
import { LOCATIONS } from './Locations';

export default {
    ...AUTH,
    ...CATEGORIES,
    ...INTERESTS,
    ...LOCATIONS,
}
axios.interceptors.request.use(
    config => {
        let accessToken = localStorage.getItem('JWT');
        if (accessToken) {
            config.headers = Object.assign({
                Authorization: accessToken,
                'Accept-Language': store.state.loca.language
            }, config.headers);
        }
        return config;
    },
    error => {
        return Promise.reject(error);
    }
);

axios.interceptors.response.use((response) => response, (error) => {
    if (error.response.status == 401) {
        store.dispatch('updateGlobalMessage', {
            text: error.response.data.errors.message,
            active: true,
        })

        localStorage.removeItem('JWT')
        localStorage.setItem('authed', false)
        store.dispatch('updateAuthed', false)
        router.push("/login");
    }
    throw error;
});