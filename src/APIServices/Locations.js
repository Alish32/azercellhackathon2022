import axios from 'axios';
import { BASE_URL } from './Variables'
const API_URL_LOCATIONS = `${BASE_URL}`

export const LOCATIONS = {
    getLocations(data) {
        const url = `${API_URL_LOCATIONS}/client/search-places`;
        return axios.post(url, data);
    },
}