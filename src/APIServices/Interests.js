import axios from 'axios';
import { BASE_URL } from './Variables'
const API_URL_INTERESTS= `${BASE_URL}`

export const INTERESTS = {
    interests(categoryId) {
        const url = `${API_URL_INTERESTS}/categories/${categoryId}/interests`;
        return axios.get(url);
    },
    saveInterest(data) {
        const url = `${API_URL_INTERESTS}/business/place`;
        return axios.post(url, data);
    },
}