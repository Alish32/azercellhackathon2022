import axios from 'axios';
import { BASE_URL } from './Variables'
const API_URL_AUTH = `${BASE_URL}`

export const AUTH = {
    registration(data) {
        const url = `${API_URL_AUTH}/registration`;
        return axios.post(url, data);
    },
    login(data) {
        const url = `${API_URL_AUTH}/login`;
        return axios.post(url, data);
    },
}