import store from '@/store/index'

export default function checkAuth(next, to) {
    const authed = store.state.authed
    if (authed && to.fullPath == '/login') {
        next('/')
    }
}