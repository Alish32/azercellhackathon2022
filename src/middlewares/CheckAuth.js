import store from '@/store/index'

export default function checkAuth(next, to) {
    const authRequired = to.matched.some((route) => route.meta.auth)
    store.dispatch('updateAuthed', localStorage.getItem('authed') == 'true')
    store.dispatch('updateUserDetails', JSON.parse(localStorage.getItem('userDetails')))
    const authed = store.state.authed
    if (authRequired && !authed) {
        next('/login')
    } else {
        next();
    }
}