import Vue from 'vue'
import VueRouter from 'vue-router'
import Main from '../views/Main.vue'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
import QRCode from '../views/QRCode.vue'
import CompanyForm from '../views/CompanyForm.vue'
import UserForm from '../views/UserForm.vue'

import checkAuth from '@/middlewares/CheckAuth.js'
import redirect from '@/middlewares/Redirect.js'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    meta: { auth: true },
    component: Main,
    children: [
      {
        path: '/',
        name: 'Home',
        meta: { auth: true },
        component: Home
      },
      {
        path: '/user-form',
        name: 'UserForm',
        meta: { auth: true },
        component: UserForm
      },
      {
        path: '/company-form',
        name: 'CompanyForm',
        meta: { auth: true },
        component: CompanyForm
      },
      {
        path: '/qr-code',
        name: 'QRCode',
        meta: { auth: true },
        component: QRCode
      },
    ]
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,

  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    }
    if (to.hash) {
      return {
        selector: to.hash
      }
    }
    return {
      x: 0,
      y: 0
    }
  }
})

router.beforeEach((to, from, next) => {
  checkAuth(next, to)
  redirect(next, to)
})
export default router
