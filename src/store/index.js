import Vue from 'vue'
import Vuex from 'vuex'

import actions from './actions'
import mutations from './mutations'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    authed: false,
    userDetails: {},
    globalMessage: {
      text: '',
      active: false
    },
    loca: 'az',

  },
  mutations: mutations,
  actions: actions,
  modules: {
  },
})
