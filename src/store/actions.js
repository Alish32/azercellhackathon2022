export default {
  updateAuthed({ commit }, payload) {
    commit('setAuthed', payload)
  },
  updateUserDetails({ commit }, payload) {
    commit('setUserDetails', payload)
  },
  updateGlobalMessage({ commit }, payload) {
    commit('setGlobalMessage', payload)
  },
  updateLoca({ commit }, payload) {
    commit('setLoca', payload)
  },
}