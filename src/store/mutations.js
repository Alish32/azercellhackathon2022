import en from '../localization/en'
import az from '../localization/az'
export default {
  setAuthed(state, payload) {
    state.authed = payload
  },
  setUserDetails(state, payload) {
    state.userDetails = payload
  },
  setGlobalMessage(state, globalMessage) {
    state.globalMessage = globalMessage
  },
  setLoca(state, payload) {
    if (payload == 'en')
      state.loca = en
    else state.loca = az
  },
}