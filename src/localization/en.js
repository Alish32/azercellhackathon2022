const en = {
    'language': 'en',
    'name': 'name',
    'register': 'Register',
    'login': 'Login',
    'password': 'Password',
    'user': 'User',
    'companyName': 'Company name',
    'fullName': 'Full name',
    'userName': 'username',
    'logOut': 'Log Out',
    'title': 'Title',
    'description': 'Description',
    'discount': 'Discount',
}

export default en