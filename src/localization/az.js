const az = {
    'language': 'az',
    'name': 'ad',
    'register': 'Qeydiyyat',
    'login': 'Daxil ol',
    'password': 'Şifrə',
    'user': 'Istifadəçi',
    'companyName': 'Şirkət adı',
    'fullName': 'Ad soyad',
    'userName': 'istifadəçi adı',
    'logOut': 'Çıxış',
    'title': 'Başlıq',
    'description': 'Açıqlama',
    'discount': 'Endirim faizi',
}

export default az
